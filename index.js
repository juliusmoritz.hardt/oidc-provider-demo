const express = require('express')
var cors = require('cors')
const app = express()
const port = 3333

app.use(cors())

app.get('/', (req, res) => {
    res.send('Hello World!')
})

const { Provider } = require('oidc-provider');
const configuration = {
    clients: [{
        client_id: 'foo',
        client_secret: 'bar',
        redirect_uris: ['http://localhost:3000/authentication/callback', 'https://watson.marienschule-intern.de'],
        grant_types: ['authorization_code'],
        token_endpoint_auth_method: 'none',
        post_logout_redirect_uris: ['http://localhost:3000/']
    }],
    cookies: {
        keys: ['c2258afe-ddd3-4d15-84da-cfa748706b64cd9072f5-14c2-470b-9a3f-7058a6634ea99078bfe8-6d8c-44c9-916a-af01da79ad5c']
    }
};

const oidc = new Provider(`http://localhost:${port}`, configuration);
const accessTokens = [];
oidc.on('access_token.saved', function(token) {
    if (token) {
        accessTokens.unshift(token.jti);
        if (accessTokens.length > 100) {
            accessTokens.length = 100;
        }
    }
})

app.get('/homegroup.json', function(req, res) {
    const authHeader = req.headers['authorization'];
    if (authHeader) {
        const authHeaderParts = authHeader.split(' ', 2);
        if (authHeaderParts.length === 2 &&
            authHeaderParts[0] &&
            authHeaderParts[0].toUpperCase() == 'BEARER' &&
            authHeaderParts[1] &&
            accessTokens.includes(authHeaderParts[1])) {
            res.json({ group: 'class2029d' }); // 5d
            return;
        }
    }

    res.status(403).json({ error: 'invalid_access_token', message: 'Invalid access token!' });
})

app.use(oidc.callback());

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})